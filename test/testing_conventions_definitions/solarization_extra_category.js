//testing what happens when you add another category relationship, both correctly and incorrectly

const builder = require(`../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let solarizationConventionSrc = JSON.parse( fs.readFileSync( `./../../output/collection/conventions/log--activity--solarization/object.json` ) );
let solarizationConvention = new builder.ConventionSchema( solarizationConventionSrc );

// ids for the examples
let solarizationLogUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let logCategoryUUID = randomUUID();
let logCategory2UUID = randomUUID();
let percentageUnitUUID = randomUUID();
//let solarizationQuantityUUID = randomUUID();

let plantAssetExampleAttributes = solarizationConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = solarizationConvention.overlays.plant_asset.validExamples[0].id;

// reading the example
let solarizationCategoriesExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    solarization_log: {
        id: solarizationLogUUID,
        attributes: {
            name: "example solarization log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                },
                {
                    type: "taxonomy_term--log_category",
                    id:""
                }
            ] }
        },
    },
    log_category: {
        id: logCategoryUUID,
        attributes: {
            name: "weed_control"
        }
    },
    log_category2: {
        id: "",
        attributes: {
            name: "harvest"
        }
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};

// testing the example
solarizationConvention.validExamples = [ solarizationCategoriesExample ];

let test = solarizationConvention.testExamples();
console.log(test);
