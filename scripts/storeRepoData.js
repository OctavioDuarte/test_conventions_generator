// This script reads the environment variables describing the repo and stores them in a place in which the docusaurus static site generator can reach them.
// Said variables are needed to generate the links to the different deliverables.

const fs = require("fs");

require("dotenv").config({path:`${__dirname}/../.env`});

let repo = process.env.REPO;
let repoGroup = process.env.REPO_GROUP;
let organization = process.env.ORGANIZATION;
let packageName = JSON.parse( fs.readFileSync(`${__dirname}/../package.json`) ).name;

if ([repo,organization].some( d => !(typeof d == "string") )) {
    let error = new Error(`Please, add the variables REPO, REPO_GROUP and ORGANIZATION to your environment, these variables will allow the system to build appropriate links for all your deliverables.`);
    throw error;
};

let data = { repo: repo, repoGroup:repoGroup, organization: organization, packageName: packageName, branch: process.env.BRANCH };

fs.writeFileSync(`${__dirname}/../scripts/docusaurus/static/repo_data.json`, JSON.stringify( data ), console.error);
