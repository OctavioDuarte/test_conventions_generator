mkdir input
node scripts/getAllSchemas.js
mkdir -p output/collection
mkdir output/collection/conventions
mkdir output/collection/overlays
mkdir output/collection/documentation
node scripts/compileAllValidators.js
bash scripts/rebuildCollection.sh
node scripts/transpilation.js
