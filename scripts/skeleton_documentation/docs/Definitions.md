## Main Concepts Involved

* **Ag Data Wallet** - The permanent, canonical (meaning, "one source of truth") location of farm management or farm recordkeeping data.  Ideally the Ag Data Wallet follows FAIR principles (Findable, accessible, interoperable, and reusable).  
* **Snapshot** - A flattened JSON format which links (using [GUIDs](https://en.wikipedia.org/wiki/Universally_unique_identifier) many entities together to describe an event or events on a farm.  A Snapshot is a useful way to share all or some of farm data from an Ag Data Wallet with someone else in a simple way (sending a JSON text object, rather sending than a full database), including planting, field, and herd level data.  This may be required for external modeling services, recommendation engines, or data transfer between systems.  A snapshot has a corresponding JSON Schema to describe it shape.  The advantage of using JSON Schema to validate these snapshots is: high-confidence validation when generating, sending and receiving data; high quality out-of-the-box tools for generating human readable documentation; and general machine-readability for easier translation to end-use data stores or external APIs.
* **Collection** - A group (array) of Conventions which describe all the ways a particular individual, group, or organization formats their farm information.  _Example: a group containing a Tillage Convention, Planting Convention, and Harvest Convention_.
* **Convention** - A schema describing a single activity, like Tillage, Lab Test, or Planting.  This single activity usually contains many linked entities, usually those entities are Overlays.  _Example, a Tillage Convention may contain a Tillage Log (describing the event) linked to a Depth Quantity (describing the depth of the plow)._
* **Overlays** - A schema describing one entity which is used inside a Convention.  _Example, the Depth Quantity described above is a Schema Overlay built on top of the Standard Quantity which requires a `label` set to `depth`.  Think of these as reusable building blocks for Conventions._
* **Input** - The items used to create the published Output, and includes the following:
  + `Collection` - The Conventions, Overlays, or other schemas pulled from external locations used to build the Output either by being passed directly to the Output or (more likely) modified to produce new Conventions in the Output.
    + `Conventions` contained in the Input Collection
    + `Overlays` contained in the Input Collection
    + `Documentation` of the Input Collection entities (the Conventions and Overlays).
  + `Validators` code to easily validate Conventions or Overlays from the Input Collection
* **Output** - Links to the newly published Outputs and includes the following:
  + `Collection` - The Conventions, Overlays, or other schemas pulled from external locations to build the Output
    + `Conventions` contained in the Output Collection
    + `Overlays` contained in the Output Collection
    + `Documentation` of the Output Collection entities (the Conventions and Overlays).
  + `Validators` code to easily validate Conventions or Overlays from the Output Collection
