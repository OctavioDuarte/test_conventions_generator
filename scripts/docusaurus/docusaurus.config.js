// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';
import { buildArtifactLink, buildDocumentationLink, buildPackageLinks } from "./../../src/repo_utilities.js";
import repo from "./static/repo_data.json";

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: 'Farm Convention Schemata Wiki',
    tagline: 'Structured data is powerful data.',
    favicon: 'img/favicon.ico',

    // Set the production url of your site here
    url: 'https://our-sci.gitlab.io',
    // Set the /<baseUrl>/ pathname under which your site is served
    // For GitHub pages deployment, it is often '/<projectName>/'
    baseUrl: `/${repo.repoGroup ? repo.repoGroup.concat('/') : "" }${repo.repo}/${ repo.branch == "staging" ? "staging_wiki" : "wiki" }`,

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'OurSci', // Usually your GitHub org/user name.
    projectName: 'FarmOs Convention Schemata', // Usually your repo name.

    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: 'en',
        locales: ['en'],
    },
    themes: ["docusaurus-json-schema-plugin"],

    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                },
                blog: {
                    showReadingTime: true,
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            }),
        ],
    ],

    themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
        // Replace with your project's social card
        image: 'img/docusaurus-social-card.jpg',
        navbar: {
            title: 'Conventions',
            logo: {
                alt: 'logo',
                src: 'img/logo.png',
            },
            items: [
                {
                    type: 'docSidebar',
                    sidebarId: 'tutorialSidebar',
                    position: 'left',
                    label: 'Documentation',
                },
                // {to: '/blog', label: 'Blog', position: 'left'},
                {
                    href: `https://gitlab.com/${repo.organization}/${repo.repoGroup ? repo.repoGroup.concat('/') : "" }${repo.repo}`,
                    label: 'GitLab',
                    position: 'right',
                },
            ],
        },
        footer: {
            style: 'dark',
            links: [
                // {
                //     title: 'Docs',
                //     items: [
                //         {
                //             label: 'Documentation',
                //             to: '/docs/intro',
                //         },
                //     ],
                // },
                {
                    title: 'Community',
                    items: [
                        {
                            label: 'Twitter',
                            href: 'https://twitter.com/our_sci',
                        },
                    ],
                },
                {
                    title: 'More',
                    items: [
                        // {
                        //     label: 'Blog',
                        //     to: '/blog',
                        // },
                        {
                            label: 'General Conventions GitLab',
                            href: 'https://gitlab.com/our-sci/software/json_schema_distribution',
                        },
                    ],
                },
            ],
            copyright: `Copyright © ${new Date().getFullYear()}. Built with Docusaurus.`,
        },
        prism: {
            theme: prismThemes.github,
            darkTheme: prismThemes.dracula,
            additionalLanguages: ['json'],
        },
    }),
};

module.exports = config;
