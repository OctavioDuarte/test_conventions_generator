#!bash
if [[ -n "${NPM_AUTH_TOKEN+set}" ]]; then
    echo "Attempting to publish package, NPM token is available in environment."
    npm config set -- '//registry.npmjs.org/:_authToken' "${NPM_AUTH_TOKEN}"
    npm publish --verbose
else
    echo "No NPM token available, package won't be published. If you intended to get your package published, get your NPM repo's token and publish it as an environment variable with key NPM_AUTH_TOKEN"
fi
